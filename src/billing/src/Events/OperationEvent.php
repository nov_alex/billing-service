<?php
declare(strict_types=1);
namespace App\Events;

use App\Service\Operation\OperationInterface;
use Symfony\Contracts\EventDispatcher\Event;

abstract class OperationEvent extends Event
{
    private OperationInterface $operation;

    public function __construct(OperationInterface $operation)
    {
        $this->operation = $operation;
    }

    public function getOperation(): OperationInterface
    {
        return $this->operation;
    }
}