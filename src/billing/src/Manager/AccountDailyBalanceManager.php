<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\AccountDailyBalance as Model;
use App\Entity\Account;
use App\Exception\NotFoundException;
use App\Exception\NotUpdateException;
use App\Repository\AccountDailyBalanceRepository;
use DateTimeImmutable;
use Doctrine\DBAL\Exception;
use Doctrine\DBAL\LockMode;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\PessimisticLockException;
use RuntimeException;

final class AccountDailyBalanceManager implements AccountDailyBalanceManagerInterface
{
    /**
     * @var EntityManagerInterface
     */
    protected EntityManagerInterface $entityManager;

    /**
     * @var AccountDailyBalanceRepository
     */
    protected AccountDailyBalanceRepository $repository;

    /**
     * @construct
     * @param EntityManagerInterface $entityManager
     * @param AccountDailyBalanceRepository $repository
     */
    public function __construct(EntityManagerInterface $entityManager, AccountDailyBalanceRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    /**
     * @param Account $user
     * @param int $openDayBalance
     * @return Model
     */
    public function create(Account $user, int $openDayBalance = 0): Model
    {
        $object = new Model();
        $object
            ->setAccount($user)
            ->setDate(new DateTimeImmutable())
            ->setOpenDayBalance($openDayBalance);

        return $object;
    }

    /**
     * @param string $id
     * @return Model|null
     */
    public function get(string $id): Model|null
    {
        return $this->repository->find($id);
    }

    /**
     * @param Account $account
     * @throws NotFoundException
     * @return Model
     */
    public function getByAccount(Account $account): Model
    {
        $object = $this->repository->findOneBy(['account' => $account->getId()]);
        if (!$object) {
            throw new NotFoundException(\sprintf('Account daily balance for user: %s not found!', $account->getId()));
        }
        return $object;
    }

    /**
     * {@inheritDoc}
     */
    public function update(Model $object): Model
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->persist($object);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
            throw new NotUpdateException(\sprintf('Fail to update User daily balance with id: %s', $object->getId()), 0, $exception);
        }

        return $object;
    }

    /**
     * @return iterable
     */
    public function findAll(): iterable
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('adb')
            ->from(Model::class, 'adb')
        ;

        yield from $queryBuilder->getQuery()->toIterable();
    }

    /**
     * @param string $action
     * @throws Exception
     * @return void
     */
    public function lockTable(string $action): void
    {
        if (!in_array($action, ['WRITE', 'READ'])) {
            throw new RuntimeException('Wrong lock mode.');
        }

        $this->entityManager->getConnection()->executeStatement(sprintf('LOCK TABLES symfony.account_daily_balance %s, symfony.account_daily_balance AS t0 READ;', $action));
    }

    /**
     * @param Model $entity
     * @throws OptimisticLockException
     * @throws PessimisticLockException
     * @return void
     */
    public function lockEntity(Model $entity): void
    {
        $this->entityManager->lock($entity, LockMode::PESSIMISTIC_READ);
    }
}
