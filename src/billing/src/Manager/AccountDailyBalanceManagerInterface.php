<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\AccountDailyBalance as Model;
use App\Entity\Account;
use App\Exception\NotUpdateException;

interface AccountDailyBalanceManagerInterface
{
    public function create(Account $user, int $openDayBalance = 0): Model;

    public function get(string $id): Model|null;

    public function getByAccount(Account $user): Model;

    public function lockTable(string $action): void;

    public function lockEntity(Model $entity): void;

    public function findAll(): iterable;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;
}