<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Account as Model;
use App\Exception\NotUpdateException;

interface AccountManagerInterface
{
    public function create(): Model;

    public function get(string $id): Model|null;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;
}