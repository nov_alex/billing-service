<?php
declare(strict_types=1);

namespace App\Manager;

use App\Entity\Transaction as Model;
use App\Entity\AccountDailyBalance;
use App\Exception\NotUpdateException;

interface TransactionManagerInterface
{
    public function create(AccountDailyBalance $userDailyBalance, string $operation, string $type, int $amount, bool $accepted = true, bool $onHold = false): Model;

    public function get(int $id): Model|null;

    public function findAccountDailyBalanceTransactions(AccountDailyBalance $accountDailyBalance, \DateTimeInterface $startDate): array;

    /**
     * @param string[] $operations
     * @return Model[]
     */
    public function findHeldOperationTransactions(array $operations): array;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;

    public function transactional(callable $func): bool;

    public function lockTable(): void;

    public function unLockTables(): void;
}