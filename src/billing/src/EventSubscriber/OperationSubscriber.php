<?php

namespace App\EventSubscriber;

use App\Events\OperationAcceptedEvent;
use App\Events\OperationDeclinedEvent;
use App\Events\OperationEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class OperationSubscriber implements EventSubscriberInterface
{
    public function onOperationAcceptedEvent(OperationEvent $event)
    {

    }

    public function onOperationDeclinedEvent(OperationEvent $event)
    {
        // ...
    }

    public static function getSubscribedEvents()
    {
        return [
            OperationAcceptedEvent::class => 'onOperationAcceptedEvent',
            OperationDeclinedEvent::class => 'onOperationDeclinedEvent',
        ];
    }
}
