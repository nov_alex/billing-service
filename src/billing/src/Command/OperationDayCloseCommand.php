<?php
declare(strict_types=1);
namespace App\Command;

use App\Service\BillingInteface;
use App\Helper\Utils;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class OperationDayCloseCommand extends Command
{
    protected static $defaultName = 'billing:operation-day:close';
    protected static $defaultDescription = 'Close operation day and recalculate accounts` daily balance';

    private BillingInteface $billing;

    public function __construct(BillingInteface $billing, string $name = null)
    {
        parent::__construct($name);
        $this->billing = $billing;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $accountBalances = $this->billing->processDailyBalance();

            $table = new Table($output);
            $table->setHeaders(['Account ID', 'Open day balance']);
            $rows = [];
            foreach ($accountBalances as $accountBalance) {
                $rows[] = [
                    $accountBalance->getAccount()->getId(),
                    Utils::convertFromMicroCurrency($accountBalance->getOpenDayBalance()),
                ];
            }
            $table
                ->setRows($rows)
                ->render()
            ;
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return 1;
        }

        return 0;
    }
}
