<?php
declare(strict_types=1);
namespace App\Messenger\Handler;

use App\Messenger\Message\UnholdOperationsMessage;
use App\Service\BillingInteface;
use App\Manager\AccountManagerInterface;
use App\Service\OperationFactoryInterface;
use App\Messenger\Message\OperationsMessage;

class UnholdOperationsHandler
{
    private BillingInteface $billing;

    public function __construct(BillingInteface $billing)
    {
        $this->billing = $billing;
    }

    public function __invoke(UnholdOperationsMessage $message)
    {
        $operations = $message->getOperations();

        $this->billing->releaseOperations($operations);
    }
}
