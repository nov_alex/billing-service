<?php
declare(strict_types=1);
namespace App\Messenger\Handler;

use App\Service\BillingInteface;
use App\Manager\AccountManagerInterface;
use App\Service\OperationFactoryInterface;
use App\Messenger\Message\OperationsMessage;
use Psr\Log\LoggerInterface;

class OperationsHandler
{
    private LoggerInterface $logger;
    private BillingInteface $billing;
    private OperationFactoryInterface $operationFactory;
    private AccountManagerInterface $accountManager;

    public function __construct(BillingInteface $billing, OperationFactoryInterface $operationFactory, AccountManagerInterface $accountManager, LoggerInterface $logger)
    {
        $this->billing = $billing;
        $this->operationFactory = $operationFactory;
        $this->accountManager = $accountManager;
        $this->logger = $logger;
    }

    public function __invoke(OperationsMessage $message)
    {
        $operations = $message->getOperations();
        $amount = $message->getAmount();
        $onHold = $message->isHold();

        $billingOperations = [];
        foreach ($operations as $operation) {
            $account = $this->accountManager->get($operation[OperationsMessage::OPERATION_MESSAGE_ACCOUNT]);
            if (!$account) {
                $errorMessage = \sprintf('Account with id %s not found!', $operation[OperationsMessage::OPERATION_MESSAGE_ACCOUNT]);
                $this->logger->error($errorMessage);
                throw new \Exception($errorMessage);
            }
            $billingOperations[] = $this->operationFactory->createOperation($account, $operation[OperationsMessage::OPERATION_MESSAGE_ACTION], $amount, $onHold);
        }

        $this->billing->process($billingOperations);
    }
}
