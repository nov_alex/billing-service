<?php
declare(strict_types=1);
namespace App\Messenger\Message;

class OperationsMessage
{
    public const OPERATION_MESSAGE_ACTION  = 'action';
    public const OPERATION_MESSAGE_ACCOUNT = 'accountId';

    /**
     * @var array
     * [
     *   [ 'action' => 'credit', 'accountId' => '<accountId>' ],
     *   [ 'action' => 'debit', 'accountId' => '<accountId_2>' ],
     * ]
     */
    private array $operations;
    private int $amount;
    private bool $hold;

    public function __construct(int $amount, array $operations = [], bool $hold = false)
    {
        $this->operations = $operations;
        $this->amount = $amount;
        $this->hold = $hold;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function isHold(): bool
    {
        return $this->hold;
    }

    /**
     * @return array
     * [
     *   [ 'action' => 'credit', 'accountId' => '<accountId>' ],
     *   [ 'action' => 'debit', 'accountId' => '<accountId_2>' ],
     * ]
     */
    public function getOperations(): array
    {
        return $this->operations;
    }

    public function addOperation(string $action, string $accountId): self
    {
        $this->operations[] = [ self::OPERATION_MESSAGE_ACTION => $action, self::OPERATION_MESSAGE_ACCOUNT => $accountId ];

        return $this;
    }
}