<?php
declare(strict_types=1);
namespace App\Messenger\Transport;

use App\Messenger\Transport\Serialization\Serializer;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\AmqpTransport;
use Symfony\Component\Messenger\Bridge\Amqp\Transport\Connection;
use Symfony\Component\Messenger\Transport\Serialization\SerializerInterface;
use Symfony\Component\Messenger\Transport\TransportFactoryInterface;
use Symfony\Component\Messenger\Transport\TransportInterface;
use Symfony\Component\Serializer\SerializerInterface as SymfonySerializerInterface;

final class TransportFactory implements TransportFactoryInterface
{
    protected SymfonySerializerInterface $serializer;

    public function __construct(SymfonySerializerInterface $serializer)
    {
        $this->serializer = $serializer;
    }

    public function createTransport(string $dsn, array $options, SerializerInterface $serializer): TransportInterface
    {
        return new AmqpTransport(Connection::fromDsn($dsn, $options), new Serializer($this->serializer, 'json', $options));
    }

    public function supports(string $dsn, array $options): bool
    {
        return str_starts_with($dsn, 'billing://');
    }
}