<?php
declare(strict_types=1);
namespace App\Service;

use App\Entity\AccountDailyBalance;
use App\Events\OperationAcceptedEvent;
use App\Events\OperationDeclinedEvent;
use App\Exception\NotFoundException;
use App\Manager\AccountManagerInterface;
use App\Manager\TransactionManagerInterface;
use App\Manager\AccountDailyBalanceManagerInterface;
use App\Service\Operation\OperationInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\Uid\Uuid;

final class Billing implements BillingInteface
{
    private LoggerInterface $logger;
    private EventDispatcherInterface $eventDispatcher;
    private AccountDailyBalanceManagerInterface $accountDailyBalanceManager;
    private TransactionManagerInterface $transactionManager;

    public function __construct(
        AccountDailyBalanceManagerInterface $accountDailyBalanceManager,
        TransactionManagerInterface $transactionManager,
        EventDispatcherInterface $eventDispatcher,
        LoggerInterface $logger
    )
    {
        $this->accountDailyBalanceManager = $accountDailyBalanceManager;
        $this->transactionManager = $transactionManager;
        $this->eventDispatcher = $eventDispatcher;
        $this->logger = $logger;
    }

    /**
     * {@inheritDoc}
     */
    public function process(array $operations): bool
    {
        $callback = function() use ($operations) {
            $operationId = Uuid::v4()->toBase32();
            /** @var OperationInterface $operation */
            foreach ($operations as $operation) {
                $this->accountDailyBalanceManager->lockTable('WRITE');
                $accountDailyBalance = $this->accountDailyBalanceManager->getByAccount($operation->getAccount());
                $dailyBalanceDate = $accountDailyBalance->getDate();

                $this->transactionManager->lockTable();
                $previousTransactions = $this->transactionManager->findAccountDailyBalanceTransactions($accountDailyBalance, $dailyBalanceDate);

                $total = $this->getTotalBalance($accountDailyBalance->getOpenDayBalance(), $previousTransactions, $operation);

                if ($total < 0) {
                    $this->eventDispatcher->dispatch(new OperationDeclinedEvent($operation));
                    return false;
                }

                $transaction = $this->transactionManager->create($accountDailyBalance, $operationId, $operation->getType(), $operation->getAmount(), true, $operation->isOnHold());
                $this->transactionManager->update($transaction);

                $this->eventDispatcher->dispatch(new OperationAcceptedEvent($operation));

                $this->transactionManager->unLockTables();
            }

            return true;
        };

        return $this->transactionManager->transactional($callback);
    }

    public function processDailyBalance(): iterable
    {
        $accountDailyBalances = $this->accountDailyBalanceManager->findAll();
        foreach ($accountDailyBalances as $accountDailyBalance) {
            /** @var AccountDailyBalance $accountDailyBalance */
            $callback = function() use ($accountDailyBalance) {
                $this->accountDailyBalanceManager->lockEntity($accountDailyBalance);
                $transactions = $this->transactionManager->findAccountDailyBalanceTransactions($accountDailyBalance, $accountDailyBalance->getDate());

                $total = $this->getTotalBalance($accountDailyBalance->getOpenDayBalance(), $transactions);
                $accountDailyBalance
                    ->setOpenDayBalance($total)
                    ->setDate(new \DateTimeImmutable())
                ;

                $this->accountDailyBalanceManager->update($accountDailyBalance);

                return true;
            };

            $this->transactionManager->transactional($callback);

            yield $accountDailyBalance;
        }
    }

    public function releaseOperations(array $operations): bool
    {
        $transactions = $this->transactionManager->findHeldOperationTransactions($operations);
        $callback = function() use ($transactions) {
            /** @var OperationInterface $operation */
            foreach ($transactions as $transaction) {
                $accountDailyBalance = $transaction->getAccountDailyBalance();
                $dailyBalanceDate = $accountDailyBalance->getDate();

                $this->transactionManager->lockTable();
                $previousTransactions = $this->transactionManager->findAccountDailyBalanceTransactions($accountDailyBalance, $dailyBalanceDate);

                $total = $this->getTotalBalance($accountDailyBalance->getOpenDayBalance(), $previousTransactions);
                if ($transaction->isCredit()) {
                    $total += $transaction->getAmount();
                } else {
                    $total -= $transaction->getAmount();
                }

                if ($total < 0) {
                    $this->eventDispatcher->dispatch(new OperationDeclinedEvent($operation));
                    return false;
                }

                $transaction
                    ->setCreatedTime(new \DateTimeImmutable())
                    ->setOnHold(false)
                ;

                $this->transactionManager->update($transaction);
                $this->eventDispatcher->dispatch(new OperationAcceptedEvent($operation));

                $this->transactionManager->unLockTables();
            }

            return true;
        };

        return $this->transactionManager->transactional($callback);
    }

    private function getTotalBalance(int $accountBalance, array $previousTransactions, OperationInterface $currentOperation = null): int
    {
        // Calculate previous balance
        foreach ($previousTransactions as $transaction) {
            if ($transaction->isCredit()) {
                $accountBalance += $transaction->getAmount();
            } else {
                $accountBalance -= $transaction->getAmount();
            }
        }

        // Add current operation amount
        if ($currentOperation) {
            $accountBalance = $currentOperation->calculate($accountBalance);
        }

        return $accountBalance;
    }
}