<?php
declare(strict_types=1);
namespace App\Service;

use App\Entity\Account;
use App\Service\Operation\OperationInterface;

interface OperationFactoryInterface
{
    public function createOperation(Account $account, string $operationType, int $amount, bool $onHold = false): OperationInterface;
}