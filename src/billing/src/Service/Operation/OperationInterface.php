<?php
declare(strict_types=1);

namespace App\Service\Operation;

use App\Entity\Account;

interface OperationInterface
{
    public function calculate(int $balance): int;

    public function getAccount(): Account;

    public function getAmount(): int;

    public function getType(): string;

    public function isOnHold(): bool;
}