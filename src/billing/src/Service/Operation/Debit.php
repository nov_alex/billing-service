<?php
declare(strict_types=1);
namespace App\Service\Operation;

use App\Service\TransactionType;

final class Debit extends Operation
{
    public function calculate(int $balance): int
    {
        return $balance - $this->getAmount();
    }

    public function getType(): string
    {
        return TransactionType::DEBIT;
    }
}
