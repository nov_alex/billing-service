<?php
declare(strict_types=1);
namespace App\Service\Operation;

use App\Entity\Account;

abstract class Operation implements OperationInterface
{
    protected Account $account;
    protected int $amount;
    protected bool $onHold;

    public function __construct(Account $account, int $amount, bool $onHold)
    {
        $this->account = $account;
        $this->amount = $amount;
        $this->onHold = $onHold;
    }

    public function getAccount(): Account
    {
        return $this->account;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function isOnHold(): bool
    {
        return $this->onHold;
    }
}