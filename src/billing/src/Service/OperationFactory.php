<?php
declare(strict_types=1);
namespace App\Service;

use App\Entity\Account;
use App\Service\Operation\Credit;
use App\Service\Operation\Debit;
use App\Service\Operation\OperationInterface;

class OperationFactory implements OperationFactoryInterface
{
    public function createOperation(Account $account, string $operationType, int $amount, bool $onHold = false): OperationInterface
    {
        if (TransactionType::CREDIT === $operationType) {
            return new Credit($account, $amount, $onHold);
        }

        if (TransactionType::DEBIT === $operationType) {
            return new Debit($account, $amount, $onHold);
        }

        throw new \Exception(\sprintf('Operation %s not found!', $operationType));
    }

}