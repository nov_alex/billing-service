<?php
declare(strict_types=1);

namespace App\Service;

use App\Service\Operation\OperationInterface;

interface BillingInteface
{
    /**
     * @param OperationInterface[] $operations
     *
     * @return bool
     */
    public function process(array $operations): bool;

    public function processDailyBalance(): iterable;

    public function releaseOperations(array $operations): bool;
}