<?php

namespace App\Entity;

use App\Repository\AccountRepository;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: AccountRepository::class)]
#[ORM\Table(name: 'account')]
class Account
{
    #[ORM\Id]
    #[ORM\GeneratedValue(strategy: 'UUID')]
    #[ORM\Column(type: 'guid')]
    private ?string $id;

    #[ORM\OneToOne(mappedBy: 'account', targetEntity: AccountDailyBalance::class, cascade: ['persist', 'remove'])]
    private ?AccountDailyBalance $accountDailyBalance;

    public function getId(): ?string
    {
        return $this->id;
    }

    public function getAccountDailyBalance(): ?AccountDailyBalance
    {
        return $this->accountDailyBalance;
    }

    public function setAccountDailyBalance(AccountDailyBalance $accountDailyBalance): self
    {
        if ($accountDailyBalance->getAccount() !== $this) {
            $accountDailyBalance->setAccount($this);
        }

        $this->accountDailyBalance = $accountDailyBalance;

        return $this;
    }
}
