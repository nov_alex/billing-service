<?php
declare(strict_types=1);
namespace Manager\Service;

class TransactionType
{
    const CREDIT = 'credit';
    const DEBIT = 'debit';

    public static function getAllTypes(): array
    {
        return [
            self::CREDIT,
            self::DEBIT,
        ];
    }
}