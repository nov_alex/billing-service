<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\Transaction as Model;
use Manager\Entity\AccountDailyBalance;
use Manager\Exception\NotUpdateException;
use Manager\Repository\TransactionRepository;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\EntityManagerInterface;

final class TransactionManager implements TransactionManagerInterface
{
    private EntityManagerInterface $entityManager;
    private TransactionRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, TransactionRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function create(AccountDailyBalance $userDailyBalance, string $type, int $amount, bool $accepted = true, bool $onHold = false): Model
    {
        $object = new Model();
        $object
            ->setAccountDailyBalance($userDailyBalance)
            ->setType($type)
            ->setAmount($amount)
            ->setAccepted($accepted)
            ->setOnHold($onHold)
            ->setCreatedTime(new \DateTimeImmutable())
        ;

        return $object;
    }

    public function get(int $id): Model|null
    {
        return $this->repository->find($id);
    }

    public function findAccountDailyBalanceTransactions(AccountDailyBalance $accountDailyBalance, \DateTimeInterface $startDate): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->eq('accountDailyBalance', $accountDailyBalance))
            ->andWhere(Criteria::expr()->andX(
                Criteria::expr()->gte('createdTime', $startDate),
                Criteria::expr()->lte('createdTime', new \DateTimeImmutable()),
                Criteria::expr()->eq('onHold', false),
            ))
        ;

        return $this->repository->matching($criteria)->toArray() ?? [];
    }

    /**
     * {@inheritDoc}
     */
    public function findHeldAccountDailyBalanceTransactions(AccountDailyBalance $accountDailyBalance): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->andX(
                Criteria::expr()->eq('accountDailyBalance', $accountDailyBalance),
                Criteria::expr()->eq('onHold', true),
            ))
        ;

        return $this->repository->matching($criteria)->toArray() ?? [];
    }

    /**
     * {@inheritDoc}
     */
    public function findHeldOperationTransactions(array $operations): array
    {
        $criteria = Criteria::create()
            ->where(Criteria::expr()->andX(
                Criteria::expr()->in('operation', $operations),
                Criteria::expr()->eq('onHold', true),
            ));

        return $this->repository->matching($criteria)->toArray() ?? [];
    }

    /**
     * {@inheritDoc}
     */
    public function update(Model $object): Model
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->persist($object);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
            throw new NotUpdateException(\sprintf('Fail to update Transaction with id: %s', $object->getId()), 0, $exception);
        }

        return $object;
    }

    public function transactional(callable $func): bool
    {
        return $this->entityManager->wrapInTransaction($func);
    }

    public function lockTable(): void
    {
        $this->entityManager->getConnection()->executeStatement('LOCK TABLES symfony.operation WRITE;');
    }
}