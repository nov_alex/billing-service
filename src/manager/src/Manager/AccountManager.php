<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\Account as Model;
use Manager\Exception\NotUpdateException;
use Manager\Repository\AccountRepository;
use Doctrine\ORM\EntityManagerInterface;

class AccountManager implements AccountManagerInterface
{
    protected EntityManagerInterface $entityManager;
    protected AccountRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, AccountRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function create(): Model
    {
        return new Model();
    }

    public function get(string $id): Model|null
    {
        return $this->repository->find($id);
    }

    /**
     * {@inheritDoc}
     */
    public function update(Model $object): Model
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->persist($object);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
            throw new NotUpdateException(\sprintf('Fail to update Account with id: %s', $object->getId()), 0, $exception);
        }

        return $object;
    }
}