<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\Account as Model;
use Manager\Exception\NotUpdateException;

interface AccountManagerInterface
{
    public function create(): Model;

    public function get(string $id): Model|null;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;
}