<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\AccountDailyBalance as Model;
use Manager\Entity\Account;
use Manager\Exception\NotFoundException;
use Manager\Exception\NotUpdateException;
use Doctrine\ORM\EntityManagerInterface;
use Manager\Repository\AccountDailyBalanceRepository;

final class AccountDailyBalanceManager implements AccountDailyBalanceManagerInterface
{
    protected EntityManagerInterface $entityManager;
    protected AccountDailyBalanceRepository $repository;

    public function __construct(EntityManagerInterface $entityManager, AccountDailyBalanceRepository $repository)
    {
        $this->entityManager = $entityManager;
        $this->repository = $repository;
    }

    public function create(Account $user, int $openDayBalance = 0): Model
    {
        $object = new Model();
        $object
            ->setAccount($user)
            ->setDate(new \DateTimeImmutable())
            ->setOpenDayBalance($openDayBalance)
        ;

        return $object;
    }

    public function get(string $id): Model|null
    {
        return $this->repository->find($id);
    }

    public function getByAccount(Account $account): Model
    {
        $object = $this->repository->findOneBy(['account' => $account->getId()]);
        if (!$object) {
            throw new NotFoundException(\sprintf('Account daily balance for user: %s not found!', $account->getId()));
        }
        return $object;
    }

    public function findAll(): iterable
    {
        $queryBuilder = $this->entityManager->createQueryBuilder();
        $queryBuilder
            ->select('adb')
            ->from(Model::class, 'adb')
        ;

        yield from $queryBuilder->getQuery()->toIterable();
    }

    /**
     * {@inheritDoc}
     */
    public function update(Model $object): Model
    {
        try {
            $this->entityManager->beginTransaction();
            $this->entityManager->persist($object);
            $this->entityManager->flush();
            $this->entityManager->commit();
        } catch (\Exception $exception) {
            $this->entityManager->rollback();
            throw new NotUpdateException(\sprintf('Fail to update User daily balance with id: %s', $object->getId()), 0, $exception);
        }

        return $object;
    }
}