<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\AccountDailyBalance as Model;
use Manager\Entity\Account;
use Manager\Exception\NotUpdateException;

interface AccountDailyBalanceManagerInterface
{
    public function create(Account $user, int $openDayBalance = 0): Model;

    public function get(string $id): Model|null;

    public function getByAccount(Account $account): Model;

    public function findAll(): iterable;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;
}