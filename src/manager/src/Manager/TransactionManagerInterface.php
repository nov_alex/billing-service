<?php
declare(strict_types=1);

namespace Manager\Manager;

use Manager\Entity\Transaction;
use Manager\Entity\Transaction as Model;
use Manager\Entity\AccountDailyBalance;
use Manager\Exception\NotUpdateException;

interface TransactionManagerInterface
{
    public function create(AccountDailyBalance $userDailyBalance, string $type, int $amount, bool $accepted = true, bool $onHold = false): Model;

    public function get(int $id): Model|null;

    public function findAccountDailyBalanceTransactions(AccountDailyBalance $accountDailyBalance, \DateTimeInterface $startDate): array;

    /**
     * @param AccountDailyBalance $accountDailyBalance
     * @return Model[]
     */
    public function findHeldAccountDailyBalanceTransactions(AccountDailyBalance $accountDailyBalance): array;

    /**
     * @param string[] $operations
     * @return Transaction[]
     */
    public function findHeldOperationTransactions(array $operations): array;

    /**
     * @throws NotUpdateException
     */
    public function update(Model $object): Model;

    public function transactional(callable $func): bool;

    public function lockTable(): void;
}