<?php

declare(strict_types=1);

namespace Manager\Command;

use Manager\Manager\TransactionManagerInterface;
use Manager\Messenger\Producer\ProducerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class UnholdOperationsCommand extends Command
{
    protected const OPERATION_IDS = 'operations';

    protected static $defaultName = 'manager:operation:unhold';
    protected static $defaultDescription = 'Unhold operation';

    protected TransactionManagerInterface $transactionManager;

    private ProducerInterface $producer;

    public function __construct(
        TransactionManagerInterface $transactionManager,
        ProducerInterface $producer,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->producer = $producer;
        $this->transactionManager = $transactionManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::OPERATION_IDS, InputArgument::REQUIRED|InputArgument::IS_ARRAY, 'Operations` ids')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $operationIds = $input->getArgument(self::OPERATION_IDS);
        $transactions = $this->transactionManager->findHeldOperationTransactions($operationIds);

        if (empty($transactions)) {
            $io->error('No held operations fount!');

            return 22;
        }

        try {
            $this->producer->produceUnholdOperationMessage($transactions);

            $io->success('Transaction was successfully produced!');
        } catch (\Exception $e) {
            $io->error($e->getMessage());

            return 1;
        }

        return 0;
    }
}
