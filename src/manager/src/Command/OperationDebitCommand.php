<?php
declare(strict_types=1);
namespace Manager\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

final class OperationDebitCommand extends OperationCommand
{
    protected static $defaultName = 'manager:operation:debit';
    protected static $defaultDescription = 'Make debit operation';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::DEBIT_ACCOUNT_ID, InputArgument::REQUIRED, 'Account id')
            ->addArgument(self::OPERATION_AMOUNT, InputArgument::REQUIRED, 'Debit operation amount')
            ->addOption(self::OPERATION_HOLD, null, InputOption::VALUE_NONE, 'Hold operation')
        ;
    }

    protected function getOperationTypes(): array
    {
        return ['debit'];
    }
}
