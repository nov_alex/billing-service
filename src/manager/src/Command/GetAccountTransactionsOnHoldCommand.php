<?php

declare(strict_types=1);

namespace Manager\Command;

use Manager\Helper\Utils;
use Manager\Manager\AccountDailyBalanceManagerInterface;
use Manager\Manager\AccountManagerInterface;
use Manager\Manager\TransactionManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class GetAccountTransactionsOnHoldCommand extends Command
{
    protected static $defaultName = 'manager:get:account-transactions-on-hold';
    protected static $defaultDescription = 'Get accounts` balance information';

    private const ACCOUNT_ID = 'accountId';

    private TransactionManagerInterface $transactionManager;
    private AccountManagerInterface $accountManager;
    private AccountDailyBalanceManagerInterface $accountDailyBalanceManager;

    public function __construct(
        AccountDailyBalanceManagerInterface $accountDailyBalanceManager,
        AccountManagerInterface $accountManager,
        TransactionManagerInterface $transactionManager,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->accountDailyBalanceManager = $accountDailyBalanceManager;
        $this->accountManager = $accountManager;
        $this->transactionManager = $transactionManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::ACCOUNT_ID, InputArgument::REQUIRED, 'Get account info')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $accountId = $input->getArgument(self::ACCOUNT_ID);
        if (!$account = $this->accountManager->get($accountId)) {
            $io->error(\sprintf('Account with id %s not found!', $accountId));
            return 22;
        }

        $accountDailyBalance = $this->accountDailyBalanceManager->getByAccount($account);

        $transactions = $this->transactionManager->findHeldAccountDailyBalanceTransactions($accountDailyBalance);

        $table = new Table($output);
        $table->setHeaders(['Account ID', 'Operation', 'Type', 'Amount', 'On Hold']);

        $rows = [];
        foreach ($transactions as $transaction) {
            $rows[] = [
                $transaction->getAccountDailyBalance()->getAccount()->getId(),
                $transaction->getOperation(),
                $transaction->getType(),
                Utils::convertFromMicroCurrency($transaction->getAmount()),
                $transaction->isOnHold()
            ];
        }
        $table
            ->setRows($rows)
            ->render()
        ;

        return 0;
    }
}
