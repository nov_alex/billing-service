<?php
declare(strict_types=1);
namespace Manager\Command;

use Symfony\Component\Console\Input\InputArgument;

final class OperationCreditCommand extends OperationCommand
{
    protected static $defaultName = 'manager:operation:credit';
    protected static $defaultDescription = 'Make credit operation';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::CREDIT_ACCOUNT_ID, InputArgument::REQUIRED, 'Account id')
            ->addArgument(self::OPERATION_AMOUNT, InputArgument::REQUIRED, 'Credit operation amount')
        ;
    }

    protected function getOperationTypes(): array
    {
        return ['credit'];
    }
}
