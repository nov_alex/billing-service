<?php

declare(strict_types=1);

namespace Manager\Command;

use Manager\Helper\Utils;
use Manager\Manager\AccountDailyBalanceManagerInterface;
use Manager\Manager\AccountManagerInterface;
use Manager\Manager\TransactionManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class GetAllAccountsInfoCommand extends Command
{
    private const ACCOUNT_ID = 'accountId';

    protected static $defaultName = 'manager:get:account-info';
    protected static $defaultDescription = 'Get accounts` balance information';

    private TransactionManagerInterface $transactionManager;
    private AccountManagerInterface $accountManager;
    private AccountDailyBalanceManagerInterface $accountDailyBalanceManager;

    public function __construct(AccountDailyBalanceManagerInterface $accountDailyBalanceManager, AccountManagerInterface $accountManager, TransactionManagerInterface $transactionManager, string $name = null)
    {
        parent::__construct($name);
        $this->accountDailyBalanceManager = $accountDailyBalanceManager;
        $this->accountManager = $accountManager;
        $this->transactionManager = $transactionManager;
    }

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::ACCOUNT_ID, InputArgument::OPTIONAL, 'Get account info');
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $accountId = $input->getArgument(self::ACCOUNT_ID);

        $accounts = [];

        if ($accountId) {
            if (!$account = $this->accountManager->get($accountId)) {
                $io->error(\sprintf('Account with id %s not found!', $accountId));

                return 22;
            }

            $accounts[] = $this->accountDailyBalanceManager->getByAccount($account);
        } else {
            $accounts = $this->accountDailyBalanceManager->findAll();
        }

        $table = new Table($output);
        $table->setHeaders(['Account ID', 'Open day balance']);

        $rows = [];
        foreach ($accounts as $account) {
            $previousTransactions = $this->transactionManager->findAccountDailyBalanceTransactions($account, $account->getDate());
            $currentBalance = $this->getTotalBalance($account->getOpenDayBalance(), $previousTransactions);
            $rows[] = [
                $account->getAccount()?->getId(),
                Utils::convertFromMicroCurrency($account->getOpenDayBalance()),
                Utils::convertFromMicroCurrency($currentBalance),
            ];
        }

        $table
            ->setRows($rows)
            ->render()
        ;

        return 0;
    }

    public function getTotalBalance(int $accountBalance, array $previousTransactions): int
    {
        // Calculate previous balance
        foreach ($previousTransactions as $transaction) {
            if ($transaction->isCredit()) {
                $accountBalance += $transaction->getAmount();
            } else {
                $accountBalance -= $transaction->getAmount();
            }
        }

        return $accountBalance;
    }
}
