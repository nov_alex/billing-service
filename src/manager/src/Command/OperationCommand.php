<?php

declare(strict_types=1);

namespace Manager\Command;

use Manager\Helper\Utils;
use Manager\Manager\AccountManagerInterface;
use Manager\Messenger\Producer\ProducerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

abstract class OperationCommand extends Command
{
    protected const DEBIT_ACCOUNT_ID = 'debitId';
    protected const CREDIT_ACCOUNT_ID = 'creditId';
    protected const OPERATION_AMOUNT = 'amount';
    protected const OPERATION_HOLD = 'hold';

    protected static $defaultName;
    protected static $defaultDescription;

    private ProducerInterface $producer;
    private AccountManagerInterface $accountManager;

    public function __construct(
        AccountManagerInterface $accountManager,
        ProducerInterface $producer,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->accountManager = $accountManager;
        $this->producer = $producer;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        $operations = [];
        foreach ($this->getOperationTypes() as $operationType) {
            $accountId = $input->getArgument(\sprintf('%sId', $operationType));

            if (!$this->accountManager->get($accountId)) {
                $io->error(\sprintf('%s account with id %s not found!', \ucfirst($operationType), $accountId));
                return 22;
            }

            $operations[] = [
                'action' => $operationType,
                'accountId' => $accountId,
            ];
        }

        $amount = (float)$input->getArgument(self::OPERATION_AMOUNT);

        if ($amount === 0.0) {
            $io->error('Wrong amount!');

            return 22;
        }

        $amount = Utils::convertToMicroCurrency($amount);

        try {
            $this->producer->produceOperationMessage(
                $operations,
                $amount,
                $input->getOption(self::OPERATION_HOLD),
            );

            $io->success('Transaction was successfully produced!');
        } catch (\Exception $e) {
            $io->error($e->getMessage());
            return 1;
        }

        return 0;
    }

    abstract protected function getOperationTypes(): array;
}
