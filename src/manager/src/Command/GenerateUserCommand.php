<?php

declare(strict_types=1);

namespace Manager\Command;

use Manager\Manager\AccountDailyBalanceManagerInterface;
use Manager\Exception\NotUpdateException;
use Manager\Manager\AccountManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

final class GenerateUserCommand extends Command
{
    protected static $defaultName = 'manager:generate-user';
    protected static $defaultDescription = 'Generate new user';

    private AccountDailyBalanceManagerInterface $accountDailyBalanceManager;
    private AccountManagerInterface $accountManager;

    public function __construct(
        AccountManagerInterface $accountManager,
        AccountDailyBalanceManagerInterface $accountDailyBalanceManager,
        ?string $name = null
    ) {
        parent::__construct($name);
        $this->accountManager = $accountManager;
        $this->accountDailyBalanceManager = $accountDailyBalanceManager;
    }

    /**
     * {@inheritDoc}
     */
    protected function configure(): void
    {
        $this->setDescription(self::$defaultDescription);
    }

    /**
     * {@inheritDoc}
     */
    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);

        try {
            $account = $this->accountManager->update($this->accountManager->create());
            $accountDailyBalance = $this->accountDailyBalanceManager->update($this->accountDailyBalanceManager->create($account));
        } catch (NotUpdateException $e) {
            $io->error($e->getMessage());
            return 1;
        }

        $io->success([
            'User has been created!',
            \sprintf('ID: %s', $account->getId()),
            \sprintf('Balance: %d', $accountDailyBalance->getOpenDayBalance()),
        ]);

        return 0;
    }
}
