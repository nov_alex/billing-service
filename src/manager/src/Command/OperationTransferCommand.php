<?php
declare(strict_types=1);
namespace Manager\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class OperationTransferCommand extends OperationCommand
{
    protected const DEBIT_ACCOUNT_ID = 'debitId';
    protected const CREDIT_ACCOUNT_ID = 'creditId';
    protected const OPERATION_AMOUNT = 'amount';

    protected static $defaultName = 'manager:operation:transfer';
    protected static $defaultDescription = 'Make transfer operation';

    protected function configure(): void
    {
        $this
            ->setDescription(self::$defaultDescription)
            ->addArgument(self::DEBIT_ACCOUNT_ID, InputArgument::REQUIRED, 'Debit account id')
            ->addArgument(self::CREDIT_ACCOUNT_ID, InputArgument::REQUIRED, 'Credit account id')
            ->addArgument(self::OPERATION_AMOUNT, InputArgument::REQUIRED, 'Transfer operation amount')
            ->addOption(self::OPERATION_HOLD, null, InputOption::VALUE_NONE, 'Hold operation')
        ;
    }

    protected function getOperationTypes(): array
    {
        return [
            'debit',
            'credit',
        ];
    }
}
