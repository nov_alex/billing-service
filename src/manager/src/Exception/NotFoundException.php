<?php
namespace Manager\Exception;

final class NotFoundException extends \Exception
{
}