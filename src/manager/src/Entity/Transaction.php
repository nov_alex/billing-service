<?php
declare(strict_types=1);

namespace Manager\Entity;

use Manager\Repository\TransactionRepository;
use Manager\Service\TransactionType;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity(repositoryClass: TransactionRepository::class)]
#[ORM\Table(name: 'transaction')]
class Transaction
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private ?int $id;

    #[ORM\Column(type: 'integer', options: ['default' => 0])]
    private int $amount = 0;

    #[ORM\Column(type: 'boolean', options: ['default' => 0])]
    private bool $onHold;

    #[ORM\Column(type: 'boolean')]
    private bool $accepted;

    #[ORM\Column(type: 'datetime')]
    private \DateTimeInterface $createdTime;

    #[ORM\ManyToOne(targetEntity: AccountDailyBalance::class, inversedBy: 'transactions')]
    #[ORM\JoinColumn(nullable: false)]
    private ?AccountDailyBalance $accountDailyBalance;

    #[ORM\Column(type: 'string', length: 255)]
    private string $type;

    #[ORM\Column(type: 'guid')]
    private string $operation;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getAmount(): int
    {
        return $this->amount;
    }

    public function setAmount(int $amount): self
    {
        $this->amount = $amount;

        return $this;
    }

    public function isOnHold(): bool
    {
        return $this->onHold;
    }

    public function setOnHold(bool $onHold): self
    {
        $this->onHold = $onHold;

        return $this;
    }

    public function getAccepted(): bool
    {
        return $this->accepted;
    }

    public function setAccepted(bool $accepted): self
    {
        $this->accepted = $accepted;

        return $this;
    }

    public function getCreatedTime(): \DateTimeInterface
    {
        return $this->createdTime;
    }

    public function setCreatedTime(\DateTimeInterface $createdTime): self
    {
        $this->createdTime = $createdTime;

        return $this;
    }

    public function getAccountDailyBalance(): ?AccountDailyBalance
    {
        return $this->accountDailyBalance;
    }

    public function setAccountDailyBalance(?AccountDailyBalance $accountDailyBalance): self
    {
        $this->accountDailyBalance = $accountDailyBalance;

        return $this;
    }

    public function getType(): string
    {
        return $this->type;
    }

    public function setType(string $type): self
    {
        $this->type = $type;

        return $this;
    }

    public function isCredit(): bool
    {
        return TransactionType::CREDIT === $this->type;
    }

    public function isDebit(): bool
    {
        return TransactionType::DEBIT === $this->type;
    }

    public function getOperation(): string
    {
        return $this->operation;
    }

    public function setOperation(string $operation): self
    {
        $this->operation = $operation;

        return $this;
    }
}
