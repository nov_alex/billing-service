<?php

declare(strict_types=1);

namespace Manager\Helper;

class Utils
{
    private const PRECISION = 1000;

    public static function convertToMicroCurrency(float $amount): int
    {
        return (int) ($amount * self::PRECISION);
    }

    public static function convertFromMicroCurrency(int $amount): float
    {
        return $amount / self::PRECISION;
    }
}
