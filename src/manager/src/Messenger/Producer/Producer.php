<?php
namespace Manager\Messenger\Producer;

use Manager\Messenger\Message\OperationsMessage;
use Manager\Messenger\Message\UnholdOperationsMessage;
use Symfony\Component\Messenger\MessageBusInterface;

final class Producer implements ProducerInterface
{
    private MessageBusInterface $messageBus;

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    public function produceOperationMessage(array $operations, int $amount, bool $onHold = false): bool
    {
        $message = new OperationsMessage($amount, [], $onHold);
        foreach ($operations as $operation) {
            $message->addOperation($operation['action'], $operation['accountId']);
        }
        $this->messageBus->dispatch($message);

        return true;
    }

    public function produceUnholdOperationMessage(array $operations): bool
    {
        $this->messageBus->dispatch(new UnholdOperationsMessage($operations));

        return true;
    }
}