<?php
namespace Manager\Messenger\Producer;

interface ProducerInterface
{
    public function produceOperationMessage(array $operations, int $amount): bool;

    public function produceUnholdOperationMessage(array $operations): bool;
}