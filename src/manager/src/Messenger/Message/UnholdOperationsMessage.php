<?php
declare(strict_types=1);
namespace Manager\Messenger\Message;

class UnholdOperationsMessage
{
    private array $operations;

    public function __construct(array $operations)
    {
        $this->operations = $operations;
    }

    public function getOperations(): array
    {
        return $this->operations;
    }
}