<?php

declare(strict_types=1);

namespace Manager\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220424223837 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE account (id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE account_daily_balance (id INT AUTO_INCREMENT NOT NULL, account_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', date DATETIME NOT NULL, open_day_balance INT DEFAULT 0 NOT NULL, UNIQUE INDEX UNIQ_EC33F2B99B6B5FBA (account_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE transaction (id INT AUTO_INCREMENT NOT NULL, account_daily_balance_id INT NOT NULL, amount INT DEFAULT 0 NOT NULL, on_hold TINYINT(1) DEFAULT \'0\' NOT NULL, accepted TINYINT(1) NOT NULL, created_time DATETIME NOT NULL, type VARCHAR(255) NOT NULL, INDEX IDX_723705D13594E26A (account_daily_balance_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8mb4 COLLATE `utf8mb4_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE INDEX account_daily_balance_id_created_time ON transaction(account_daily_balance_id, created_time)');
        $this->addSql('ALTER TABLE account_daily_balance ADD CONSTRAINT FK_EC33F2B99B6B5FBA FOREIGN KEY (account_id) REFERENCES account (id)');
        $this->addSql('ALTER TABLE transaction ADD CONSTRAINT FK_723705D13594E26A FOREIGN KEY (account_daily_balance_id) REFERENCES account_daily_balance (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE account_daily_balance DROP FOREIGN KEY FK_EC33F2B99B6B5FBA');
        $this->addSql('ALTER TABLE transaction DROP FOREIGN KEY FK_723705D13594E26A');
        $this->addSql('DROP TABLE account');
        $this->addSql('DROP TABLE account_daily_balance');
        $this->addSql('DROP TABLE transaction');
    }
}
