FROM php:8.0-fpm-alpine as linux_dependencies

COPY wait-for-it.sh /usr/bin/wait-for-it

RUN chmod +x /usr/bin/wait-for-it

# Install modules
RUN apk upgrade --update && apk --no-cache add \
    $PHPIZE_DEPS \
    icu-dev \
    libpq \
    curl-dev \
    oniguruma-dev \
    unzip \
    rabbitmq-c \
    rabbitmq-c-dev \
    bash \
    git

FROM linux_dependencies AS php_installation

# Extract PHP source
# Create directory for amqp extension
# Download AMQP master branch files to extension directory
# Install amqp extension using built-in docker binary
RUN docker-php-source extract \
    && mkdir /usr/src/php/ext/amqp \
    && curl -L https://github.com/php-amqp/php-amqp/archive/master.tar.gz | tar -xzC /usr/src/php/ext/amqp --strip-components=1

RUN docker-php-ext-install \
    intl \
    opcache \
    mbstring \
    amqp

FROM php_installation AS php_extentions_installation

#        amqp-1.10.2 \
RUN docker-php-ext-install \
        pdo \
        pdo_mysql \
    && docker-php-ext-enable \
        amqp

RUN docker-php-source delete

COPY --from=composer /usr/bin/composer /usr/bin/composer

WORKDIR /var/www

CMD composer install ; bin/console messenger:setup-transports ; php-fpm

EXPOSE 9000
