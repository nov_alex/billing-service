# Billing

## Stack
PHP 8.0.18, Symfony 5.4, Maria DB, Rabbit MQ

## Description
Project consists of 2 services: manager and billing.
* Manager service (Symfony 5.4) allows you to control billing service through message broker (Rabbit MQ) with commands and to look for results in database (Maria DB).
* Billing service (Symfony 5.4) handles incoming operations and save them to db.

There 3 tables in database:
* account - user accounts,
* account_daily_balance - daily account balance, which has to be recalculated once a day,
* transactions - all successful transactions.

Process:
1. When user executes any operation in php-console service, it goes through the message broker to the billing service.
2. At the billing service:
   1. Take the account balance on the start of the day and the time of it from account_daily_balance table,
   2. Take all operations of the account from the start of the day (time from account_daily_balance),
   3. Process it with amount of current operation,
   4. If it is lower than 0, it will be declined, if bigger/equal - than accepted and added to db.
   5. Once a day all account balances are recalculated with all operations for that day.

## Index

* [Installation](#installation)
* [Usage](#usage)

## Installation

1. Clone repository
```
git clone https://gitlab.com/nov_alex/billing-service.git
```

2. Copy .env and rename it to .env.local
```
cd src/billing
cp .env.dist .env

cd src/manager
cp .env.dist .env
```

3. Start docker
```
cd docker

docker-compose up --build
```

## Usage

1. Run php-console and billing containers into different windows
```
#1
cd docker 
docker-compose run php-console bash

#2
cd docker 
docker-compose run billing bash
```
2. Start consumer in the billing service window
```
bin/console messenger:consume
```
3. Rum migrations
```
bin/console doctrine:migrations:migrate
```
4. Let`s make some accounts. Go to the php-console service window. And execute at least 2 times to make 2 accounts.
```
bin/console manager:generate-user
```
4. To watch information about accounts, execute:
```
bin/console manager:get:account-info
```
5. There 3 possible operations: credit, debit and transfer. Transfer operation consists of debit for one account and credit - for another.
```
Debit:
bin/console manager:operation:debit <account_id> <amount>

Credit:
bin/console manager:operation:credit <account_id> <amount>

Transfer:
bin/console manager:operation:credit <debit_account_id> <credit_account_id> <amount>
```
6. There is also a command, which can help you to recalculate all day operations and refresh account balance:
```
bin/console billing:operation-day:close
```